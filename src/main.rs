mod cli;

use clap::Parser;
use piglog_netget_config::piglog_netget_config;
use piglog::prelude::*;
use fspp::*;

#[derive(PartialEq, Eq)]
enum ExitCode {
    Success,
    Fail,
}

fn main() {
    if app() == ExitCode::Fail {
        std::process::exit(1);
    }
}

fn app() -> ExitCode {
    let args = cli::Cli::parse();

    let destination: String = match args.destination {
        Some(s) => s,
        None => std::env::current_dir().unwrap().display().to_string(),
    };

    let file_name = match netget::get_file_name_from_url(&args.url) {
        Some(s) => s,
        None => {
            piglog::fatal!("Failed to get file name from url! (Probably an invalid url!)");

            return ExitCode::Fail;
        },
    };

    let dest_path = Path::new(&format!("{}/{}", destination, file_name));

    if dest_path.exists() {
        piglog::fatal!("File already exists!");

        return ExitCode::Fail;
    }

    piglog::info!("Downloading: '{}' to: '{}'", file_name, dest_path.to_string());

    match netget::download_file(&args.url, destination, &piglog_netget_config()) {
        Ok(_) => (),
        Err(_) => {
            piglog::fatal!("Failed to download file!");

            if dest_path.exists() {
                match fs_action::delete(&dest_path) {
                    Ok(_) => (),
                    Err(_) => {
                        piglog::fatal!("Failed to delete unfinished file: {}", dest_path.to_string());
                        piglog::fatal!("Please delete it manually!");
                    },
                };
            }

            return ExitCode::Fail;
        },
    };

    piglog::success!("Downloaded file successfully!");

    return ExitCode::Success;
}
