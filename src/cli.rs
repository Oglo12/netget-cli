use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
/// Download files from the internet
pub struct Cli {
    pub url: String,
    pub destination: Option<String>,
}
