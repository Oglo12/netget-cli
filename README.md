# NetGet CLI

A program built in Rust for downloading files from the internet. (Uses the `netget` crate.)

# Usage:
```
netget-cli <URL> [DESTINATION]
```
